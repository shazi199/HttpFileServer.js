#HttpFileServer.js
##这是什么？
这是一个Node.js实现的基于Http的文件下载服务器。
目前它仅提供最简单的功能，方便做文件共享使用。
##怎么运行？
首先你需要安装好Node.js，你可以去<http://nodejs.org/>获得它。

其次你需要Node.js的包管理器NPM。
如果你是用的Windows的MSI安装包安装，则默认选项中已经安装好了NPM。
Linux用户则需要下载<http://npmjs.org/install.sh>然后运行之。

以上环境均安装完毕后，你可以在项目的根目录（即package.json所在目录）运行命令：

    npm install

然后运行

    npm start

即可。

默认情况下使用的是80端口进行服务（因此Linux用户需要使用root权限来建立服务），
以及根目录下的root目录作为资源目录。
##有参数吗？
服务器运行参数的配置文件是serverConfig.json

    port:指定端口
    root:指定资源目录，以HttpFileServer.js所在的相对目录
    loggerlevel:指定记录日志的等级，这个设置可以参考log4js
    buffsize:下载文件时读写缓存大小，默认为1048576

服务器运行使用的日志配置文件在loggerConfig.json，详细配置请参考log4js